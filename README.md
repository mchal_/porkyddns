# PorkyDDNS
Simple Porkbun DDNS Client

## 💡 Features

- Support for multiple subdomains on multiple domains
- Easily and quickly configurable, with cron-able error reporting


## 💾 Installation

**Install PorkyDDNS using `pip`**
```bash
  $ pip install porkyddns
  $ porkyddns config | sudo tee -a /etc/porkyddns.toml
  $ sudo $EDITOR /etc/porkyddns.toml # Fill in with your values!
  $ porkyddns
```

## 🔨 Usage

- Follow the [Installation](#💾-installation) instructions and fill your your desired config
- Run the `porkyddns` script on a cron job or systemd timer however often you want!


## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)

